$(document).ready(function() {
  $('#searchForm').on('submit', (e) =>  {
    e.preventDefault();
    var searchText =  $('#searchText').val();
    getMovies(searchText);     
  })


});

function getMovies(searchText){
      axios.get('http://www.omdbapi.com/?s='+searchText)
      .then(function (response) {
        let movies = response.data.Search;
        let output ='';
        $.map( movies, (movie, index) => {
          console.log(movie);
          output += `
            <div class=" col-sm-6 col-md-3 ">
              <div class="well">
              <img src="${movie.Poster}", alt="${movie.Title}" >
              <h2>${movie.Title}</h2>
              <a href="movie.html" onclick="selectedMovie('${movie.imdbID}')" class="btn btn-default">View Movie Details</a> 
              </div>
            </div>
          `
        });
        console.log(movies);
        $('#movies').html(output);
      })
      .catch(function (error) {
        console.log(error);
      });
 }

 function selectedMovie(id){
   sessionStorage.setItem('movieId', id);
   window.location = "movie.html";
 }

 function getMovie(){
  let movieId = sessionStorage.getItem('movieId');
   axios.get('http://www.omdbapi.com/?i='+movieId)
      .then(function (response) {
        console.log(response);
        let movie = response.data;
        let output ='';
        output += ` 
          <div class="row well">      
                  
              <div class="col-md-4">
                 
                    <img src="${movie.Poster}" alt="${movie.Title}" class="thumbnail">
                  
              </div> 
                <div class="col-md-8">
                 
                    <h2>${movie.Title} - ${movie.Year}</h2>
                    <ul class="list-group">
                      <li class="list-group-item"><strong>Genre : </strong> ${movie.Genre}</li>
                      <li class="list-group-item"><strong>Actors : </strong> ${movie.Actors}</li>
                      <li class="list-group-item"><strong>Writer : </strong> ${movie.Writer}</li>
                      <li class="list-group-item"><strong>Director : </strong> ${movie.Director}</li>
                      <li class="list-group-item"><strong>Awards : </strong> ${movie.Awards}</li>
                      <li class="list-group-item"><strong>Rating : </strong> ${movie.imdbRating}</li>
                      <li class="list-group-item"><strong>Votes : </strong> ${movie.imdbVotes}</li>
                    </ul>
              </div> 
          </div>
        <div class="row">
          <div class="well">
          <h3>PLot</h3>
            <p>${movie.Plot}</p>
            <hr>
            <a href="http://imdb.com/title/${movie.imdbID}" target="_blanck" class="btn btn-primary">View IMDB</a>
            <a href="index.html" class="btn btn-default">Go back to search</a>
          </div>
        </div>
      </div>

          `
        console.log(movie);
        $('#movie').html(output);
      })
      .catch(function (error) {
        console.log(error);
      });

 }
